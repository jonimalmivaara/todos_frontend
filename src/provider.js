import React, { useState } from 'react';

const OwnerContext = React.createContext({ name: '', token: '', auth: false });

const OwnerProvider = ({ children }) => {
  const [owner, setOwner] = useState({ name: '', token: '', auth: true });

  const login = (name, token) => {
    setOwner((owner) => ({
      name: name,
      token: token,
      auth: true
    }));
  };

  const logout = () => {
    setOwner((owner) => ({
      name: '',
      token: '',
      auth: false
    }));
  };

  return (
    <OwnerContext.Provider value={{ owner, login, logout }}>
      {children}
    </OwnerContext.Provider>
  );
}

  export default OwnerProvider;
  export {OwnerContext}