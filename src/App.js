import "./App.css";
import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import axios from "axios";
import OwnerProvider, { OwnerContext } from "./provider";

function ToDoFormAndList() {
  const [itemText, setItemText] = useState("");
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const { owner } = React.useContext(OwnerContext);
  const history = useHistory();
  //const [error, setError] = useState('')

  // load todo list items
  useEffect(() => {
    // start loading data, if owner is logged in
    if (owner.auth) {
      axios
        .get("http://localhost:3000/owners/" + owner.id, {
          headers: {
            Authorization: `bearer ${owner.token}`,
          },
        })
        .then((response) => {
          setLoading(false);
          setItems(response.data.todos);
        });
    } else {
      // if not logged, go back to login screen
      history.replace("/");
    }
  }, [owner, history]); // called only once

  // add a new item
  const handleSubmit = (event) => {
    // prevent normal submit event
    event.preventDefault();
    if (owner.auth) {
      axios
        .post(
          "http://localhost:3000/todos/",
          { text: itemText },
          {
            headers: {
              Authorization: `bearer ${owner.token}`,
            },
          }
        )
        .then((response) => {
          //console.log(response.data._id)
          const id = response.data._id;
          setItems([...items, { _id: id, text: itemText }]);
        });
    }
    setItemText("");
  };

  // remove item
  const removeItem = (id) => {
    //console.log(id)
    if (owner.auth) {
      axios
        .delete("http://localhost:3000/todos/" + id, {
          headers: {
            Authorization: `bearer ${owner.token}`,
          },
        })
        .then((response) => {
          // you SHOULD check server errors here...
          // filter removed todo and update UI
          const newItems = items.filter(
            (item) => item._id !== response.data._id
          );
          setItems(newItems);
        });
      // .catch(error => {
      //   if (error.response.status === 401) {
      //     setError(error.response.data.error);
      //   } else {
      //     setError("Unknow error");
      //   }
      // })
    }
  };

  const handleLogout = () => {
    history.replace("/");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={itemText}
          onChange={(event) => setItemText(event.target.value)}
          placeholder="Write a new todo here"
        />
        <input type="submit" value="Add" />
      </form>
      <ul>
        {items.map((item) => (
          <li key={item._id}>
            {item.text + " "}{" "}
            <span onClick={() => removeItem(item._id)}> x </span>
          </li>
        ))}
      </ul>
      <input type="button" value="Logout" onClick={() => handleLogout()} />
    </div>
  );
}

function Banner() {
  return <h1>Todo Example with React</h1>;
}

function Login() {
  const [error, setError] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const { login } = React.useContext(OwnerContext);

  const onChangeHandler = (event) => {
    const { name, value } = event.currentTarget;
    if (name === "name") setName(value);
    else if (name === "password") setPassword(value);
  };

  const history = useHistory();

  const handleLogin = () => {
    axios
      .post("http://localhost:3000/login", {
        owner: name,
        password: password,
      })
      .then((response) => {
        login(name, response.data.token);
        history.push("/home");
      })
      .catch((error) => {
        if (error.response.status === 401) {
          setError(error.response.data.error);
        } else {
          setError("Unknow error");
        }
      });
  };

  return (
    <div>
      <Banner />
      <form>
        <table>
          <tbody>
            <tr>
              <td>Name:</td>
              <td>
                <input
                  type="text"
                  placeholder=""
                  name="name"
                  onChange={(event) => onChangeHandler(event)}
                />
              </td>
            </tr>
            <tr>
              <td>Password:</td>
              <td>
                <input
                  type="password"
                  placeholder=""
                  name="password"
                  onChange={(event) => onChangeHandler(event)}
                />
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="button"
                  value="Login"
                  onClick={() => handleLogin()}
                />
              </td>
              <td>
                <span className="Error">{error}</span>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  );
}

function Home() {
  return (
    <div>
      <Banner />
      <ToDoFormAndList />
    </div>
  );
}

function App() {
  return (
    <OwnerProvider>
      <Router>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route path="/home">
            <Home />
          </Route>
        </Switch>
      </Router>
    </OwnerProvider>
  );
}

export default App;
